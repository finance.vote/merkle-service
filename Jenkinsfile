pipeline {
  environment {
    registryUrl =  "${env.DOCKER_REGISTRY_URL}"
    registry = "${env.DOCKER_REGISTRY_NAME}"
    tag =  'latest'//"${env.GIT_TAG_NAME}"
    registryCredential = 'registry-netisoft'
    serviceName = "${env.SERVICE_NAME}"
    webhookUrl = "${env.DEPLOY_WEBHOOK_URL}"
    dockerImage = ''
  }

  agent none

  post {
    failure {
      updateGitlabCommitStatus name: 'build', state: 'failed'
    }
    success {
      updateGitlabCommitStatus name: 'build', state: 'success'
    }
  }

  stages {
    stage('Notify Gitlab') {
      steps {
        echo 'Notify GitLab'
        updateGitlabCommitStatus name: 'build', state: 'pending'
      }
    }

    stage('Install dependencies') {
      agent {
        docker {
          image 'netisoft/node:18.12.0'
        }
      }
      steps {
        script {
          dir("${env.WORKSPACE}") {
            sh 'rm -rf node_modules'
            sh 'npm install'
          }
        }
      }
    }

    stage('Build docker image') {
      agent any
      steps {
        script {
          println "  \n - registry url: ${registryUrl} \n - registry (image) name: ${registry} \n - webhook url: ${webhookUrl} \n - service name: ${serviceName} \n - image tag: ${tag}"
            dir("${env.WORKSPACE}") {
                dockerImage = docker.build registry + ":$tag"
            }
        }
      }
    }

    stage("Pushing image to registry") {
      agent any
      when {
        expression {env.GIT_BRANCH ==~ /(origin\/(test))/}
      }
      steps {
        script {
          docker.withRegistry(registryUrl, registryCredential ) {
            dockerImage.push()
          }
        }
      }
    }

    stage("Deploy to swarm environment") {
      when {
        expression {env.GIT_BRANCH ==~ /(origin\/(test))/}
      }
      agent any
      steps {
        script {
          def response = httpRequest httpMode: 'POST', url: webhookUrl
          if(response.status >= 400){
            throw new Exception("Unable to deploy $serviceName. Response code: ${response.status}")
          }
        }
      }
    }
  }
}
