const MerkleVestingV2 = {
  contractName: "MerkleVesting",
  abi: [
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "AccountEmpty",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "AccountStillLocked",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "bytes32", name: "leaf", type: "bytes32" },
      ],
      name: "AlreadyInitialized",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "bytes32", name: "leaf", type: "bytes32" },
        { internalType: "bytes32[]", name: "proof", type: "bytes32[]" },
      ],
      name: "BadProof",
      type: "error",
    },
    {
      inputs: [{ internalType: "uint256", name: "treeIndex", type: "uint256" }],
      name: "BadTreeIndex",
      type: "error",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "UninitializedAccount",
      type: "error",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "treeIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "newRoot",
          type: "bytes32",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "ipfsHash",
          type: "bytes32",
        },
      ],
      name: "MerkleRootAdded",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "treeIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "amount",
          type: "uint256",
        },
      ],
      name: "TokensDeposited",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "treeIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "uint256",
          name: "trancheIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "recipient",
          type: "address",
        },
        {
          indexed: false,
          internalType: "bytes32",
          name: "leaf",
          type: "bytes32",
        },
      ],
      name: "TrancheInitialized",
      type: "event",
    },
    {
      anonymous: false,
      inputs: [
        {
          indexed: true,
          internalType: "uint256",
          name: "treeIndex",
          type: "uint256",
        },
        {
          indexed: true,
          internalType: "address",
          name: "destination",
          type: "address",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "numTokens",
          type: "uint256",
        },
        {
          indexed: false,
          internalType: "uint256",
          name: "tokensLeft",
          type: "uint256",
        },
      ],
      name: "WithdrawalOccurred",
      type: "event",
    },
    {
      inputs: [
        { internalType: "bytes32", name: "newRoot", type: "bytes32" },
        { internalType: "bytes32", name: "ipfsHash", type: "bytes32" },
        {
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        { internalType: "uint256", name: "tokenBalance", type: "uint256" },
      ],
      name: "addMerkleRoot",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "value", type: "uint256" },
      ],
      name: "depositTokens",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "bytes32", name: "leaf", type: "bytes32" },
      ],
      name: "getInitialized",
      outputs: [{ internalType: "bool", name: "", type: "bool" }],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "getTranche",
      outputs: [
        { internalType: "address", name: "", type: "address" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
        { internalType: "uint256", name: "", type: "uint256" },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "address", name: "destination", type: "address" },
        { internalType: "uint256", name: "totalCoins", type: "uint256" },
        { internalType: "uint256", name: "startTime", type: "uint256" },
        { internalType: "uint256", name: "endTime", type: "uint256" },
        {
          internalType: "uint256",
          name: "lockPeriodEndTime",
          type: "uint256",
        },
        { internalType: "bytes32[]", name: "proof", type: "bytes32[]" },
      ],
      name: "initialize",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      stateMutability: "nonpayable",
      type: "function",
    },
    {
      inputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      name: "merkleTrees",
      outputs: [
        { internalType: "bytes32", name: "rootHash", type: "bytes32" },
        { internalType: "bytes32", name: "ipfsHash", type: "bytes32" },
        {
          internalType: "address",
          name: "tokenAddress",
          type: "address",
        },
        {
          internalType: "uint256",
          name: "tokenBalance",
          type: "uint256",
        },
        {
          internalType: "uint256",
          name: "numTranchesInitialized",
          type: "uint256",
        },
      ],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [],
      name: "numTrees",
      outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
      stateMutability: "view",
      type: "function",
    },
    {
      inputs: [
        { internalType: "uint256", name: "treeIndex", type: "uint256" },
        { internalType: "uint256", name: "trancheIndex", type: "uint256" },
      ],
      name: "withdraw",
      outputs: [],
      stateMutability: "nonpayable",
      type: "function",
    },
  ],
  networks: {
    43113: {
      address: "0x18EAdd6bd505524a972133399ad8b72730c8c9CC",
      fromBlock: 16726148,
    },
    5: {
      address: "0x0fc8074Ebb7f0317b3E41bA21615e14Fa2254c15",
      fromBlock: 7744270,
    },
  },
};

if (process.env.NODE_ENV === "test") {
  MerkleVestingV2.networks = {
    // 4: {
    //   address: "0xD49026f83249150EaCC4D999Ee540f927163D16A",
    //   fromBlock: 11084596,
    // },
    5: {
      address: "0x0fc8074Ebb7f0317b3E41bA21615e14Fa2254c15",
      fromBlock: 7744270,
    },
  };
}

module.exports = MerkleVestingV2;
