const cron = require("node-cron");
const mongoose = require("mongoose");
const MerkleTreeModel = require("../models/MerkleTree");

const { env } = process;
const days = env.CLEAR_SHOWCASE_DATA_DAYS;

async function cleanTestData() {
  const db = mongoose.connection;
  const collectionName = env.DB_COLLECTION_NAME;
  const periodToClean = new Date(Date.now() - days * 1000 * 60 * 60 * 24);

  const result = await db.collection(collectionName).deleteMany({
    createdAt: { $gt: periodToClean } 
  });

  console.log(`test data cleaned from the last ${days} days, result: `, result);
}

async function cleanNotActivatedTrees() {
  const dateHourAgo = new Date(Date.now() - 1000 * 60 * 60 * 4);
  const result = await MerkleTreeModel.deleteMany({
    treeIndex: { $exists: false },
    createdAt: { $lt: dateHourAgo },
  });
  console.log("Not activated trees cleaned, result: ", result);
}

module.exports.startCleanerService = async () => {
  if(env.NODE_ENV === 'showcase' && days.length > 0){
    cron.schedule(`0 * */${days} * *`, cleanTestData);
  }

  cron.schedule("0 */4 * * *", cleanNotActivatedTrees);
};
