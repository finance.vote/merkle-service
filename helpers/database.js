const Project = require("../models/Project");

async function storeNewProject(project) {
  try {
    const { _id } = await Project.create(project);
    return _id;
  } catch (error) {
    return Promise.reject(error);
  }
}

async function getProject(project) {
  try {
    const keyRegExp = new RegExp(project.key, "i");
    const query = {
      $or: [{ key: keyRegExp }, { _id: project._id }],
    };
    const existingProject = await Project.findOne(query, { _id: 1 });
    if (existingProject) {
      return existingProject._id;
    } else {
      return storeNewProject(project);
    }
  } catch (error) {
    console.log(error);
    return Promise.reject(error);
  }
}

module.exports = {
  getProject,
};
