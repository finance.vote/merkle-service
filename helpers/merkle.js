const { utils, BigNumber } = require("ethers");
const fs = require("fs");
const mongoose = require("mongoose");
const { fromWei, toWei, parseUnit } = require("./utils");

const genRanHex = (size) =>
  [...Array(size)]
    .map(() => Math.floor(Math.random() * 16).toString(16))
    .join("");

const zeroHash =
  "0x0000000000000000000000000000000000000000000000000000000000000000";

const keysForCalcTreeBalance = ["minTotalPayments", "totalCoins", "value"];
const commissionCalcTreeBalance = ["maxTotalPayments", "totalCoins", "value"];
const commissionFeePct = Number(process.env.COMMISSION_PERCENTAGE) ?? 1;
const commissionAccountAddress = process.env.COMMISSION_ADDRESS;

function parentHash(a, b) {
  if (a < b) {
    return [utils.keccak256(a + b.slice(2)), false];
  } else {
    return [utils.keccak256(b + a.slice(2)), true];
  }
}
function createMerkleProof(tree, target, targetKey) {
  // TODO: this is linear, probably should make it a hash map to make it constant
  let leaf;
  if (targetKey.length)
    leaf = tree.nodes.find(
      (x) => x.data && x.data[targetKey].toLowerCase() == target.toLowerCase()
    );
  else leaf = tree.nodes.find((x) => x.hash == target);
  let x = { ...leaf };
  const proof = [];
  while (x.parentIndex > -1) {
    const parent = tree.nodes[x.parentIndex];
    const flip = x.index === parent.leftChildIndex;
    if (flip) {
      proof.push(tree.nodes[parent.rightChildIndex].hash);
    } else {
      proof.push(tree.nodes[parent.leftChildIndex].hash);
    }
    x = parent;
  }
  // console.log({ leaf, proof })
  return proof;
}

function getLeafHash(obj, hashTypes, hashKeys) {
  return utils.keccak256(
    utils.defaultAbiCoder.encode(
      hashTypes,
      hashKeys.map((x) => obj[x])
    )
  );
}

function checkMerkleProof(proof, root, obj, hashTypes, hashKeys) {
  let a = getLeafHash(obj, hashTypes, hashKeys);
  for (let i = 0; i < proof.length; i++) {
    const b = proof[i];
    // console.log('proof step', { a, b })
    const p = parentHash(a, b);
    a = p[0];
  }
  const correct = a === root;
  // console.log({ correct, a, root })
  return correct;
}

function truncateDecimalPlaces(stringValue, maxDecimalDigits) {
  if (stringValue.includes(".")) {
    const parts = stringValue.split(".");
    return parts[0] + "." + parts[1].slice(0, maxDecimalDigits);
  }

  return stringValue;
}

const parseFieldsPerContract = (fields, friendlyValues, decimalPlaces) => {
  const result = [];
  const toTruncateDecimal = decimalPlaces !== 18;

  for (const field of fields) {
    if (friendlyValues) {
      const value = toTruncateDecimal
        ? truncateDecimalPlaces(field, decimalPlaces)
        : field.toString();
      result.push(toWei(value));
      continue;
    }
    if (toTruncateDecimal) {
      const fieldNum = Number(fromWei(field));
      const value = truncateDecimalPlaces(fieldNum.toString(), decimalPlaces);
      result.push(toWei(value));
      continue;
    }
    result.push(field);
  }

  return result;
};

const parseLeavesValues = (leaves, formType, friendlyValues, decimalPlaces) => {
  let parsedData = [];

  for (const leaf of leaves) {
    if (formType === "resistor") {
      const { destination, minTotalPayments, maxTotalPayments } = leaf;

      if (!destination || !minTotalPayments || !maxTotalPayments) continue;

      const [_minTotalPayments, _maxTotalPayments] = parseFieldsPerContract(
        [minTotalPayments, maxTotalPayments],
        friendlyValues,
        decimalPlaces
      );

      parsedData.push({
        destination,
        minTotalPayments: _minTotalPayments,
        maxTotalPayments: _maxTotalPayments,
      });
    } else if (formType === "airdrop") {
      const { destination, value } = leaf;

      if (!destination || !value) continue;

      const [_value] = parseFieldsPerContract(
        [value],
        friendlyValues,
        decimalPlaces
      );

      parsedData.push({
        destination,
        value: _value,
      });
    } else {
      const { destination, totalCoins, startTime, endTime, lockPeriodEndTime } =
        leaf;

      let [_totalCoins] = parseFieldsPerContract(
        [totalCoins],
        friendlyValues,
        decimalPlaces
      );

      parsedData.push({
        destination,
        totalCoins: _totalCoins,
        startTime,
        endTime,
        lockPeriodEndTime,
      });
    }
  }

  return parsedData;
};

const getUniqueLeaves = (leaves, keys) => {
  return Object.values(
    leaves.reduce((prev, next) => {
      const newItemKey = keys.map((key) => next[key]).join("-");
      prev[newItemKey] = next;
      return prev;
    }, {})
  );
};

const minimalDateDiff = 8 * 60 * 60; // hours : minutes : seconds
const validateLeaf = (leaf, type) => {
  const isAddr = utils.isAddress(leaf.destination);
  if (!isAddr) return false;
  switch (type) {
    case "resistor": {
      return leaf.maxTotalPayments > leaf.minTotalPayments;
    }
    case "vesting": {
      return (
        leaf.endTime > leaf.lockPeriodEndTime && //end after cliffdate
        leaf.endTime - leaf.startTime > minimalDateDiff && //min vesting time
        leaf.endTime > Date.now() / 1000
      );
    }
    default: {
      return true;
    }
  }
};

const validateLeavesData = (leaves, type) => {
  for (let leaf of leaves) {
    const leafValidated = validateLeaf(leaf, type);
    if (!leafValidated) return false;
  }
  return true;
};

const isValidLeaves = (leaves, formType) => {
  let uniques = [];

  switch (formType) {
    case "resistor": {
      const keys = ["destination", "minTotalPayments", "maxTotalPayments"];
      uniques = getUniqueLeaves(leaves, keys);
      break;
    }
    case "airdrop": {
      const keys = ["destination", "value"];
      uniques = getUniqueLeaves(leaves, keys);
      break;
    }
    default: {
      const keys = [
        "destination",
        "totalCoins",
        "startTime",
        "endTime",
        "lockPeriodEndTime",
      ];
      uniques = getUniqueLeaves(leaves, keys);
      break;
    }
  }
  const validatedLeaves = validateLeavesData(leaves, formType);
  let errorMsgs = [];
  if (!validatedLeaves) errorMsgs.push("Leaves validation failed");
  if (uniques.length !== leaves.length)
    errorMsgs.push("There are duplicated values in merkle elements");
  const error = errorMsgs.join(",");
  return error;
};

function createMerkleTree(leaves, hashTypes, hashKeys) {
  let numNodes = 0;
  const keyForCalc = keysForCalcTreeBalance.find((value) =>
    hashKeys.includes(value)
  );
  const commissionKey = commissionCalcTreeBalance.find((key) =>
    hashKeys.includes(key)
  );

  //calc Tree balance with maxValue for resistor contract
  //for rest contracts min === max, overwritten at the end of commission leaf.
  const { min: minTreeBalance, max: maxTreeBalance } = leaves.reduce(
    ({ max, min }, curr) => {
      return {
        max: max.add(BigNumber.from(curr[commissionKey])),
        min: min.add(BigNumber.from(curr[keyForCalc])),
      };
    },
    { max: BigNumber.from(0), min: BigNumber.from(0) }
  );
  let treeBalance = minTreeBalance;
  let commissionLeaf = [];
  if (commissionAccountAddress) {
    const commissionFeeMin = minTreeBalance
      .mul(commissionFeePct)
      .div(100)
      .toString();
    const commissionFeeMax = maxTreeBalance
      .mul(commissionFeePct)
      .div(100)
      .toString();
    commissionLeaf = {
      ...leaves[0],
      destination: commissionAccountAddress,
      [keyForCalc]: commissionFeeMin.toString(),
      [commissionKey]: commissionFeeMax.toString(),
    };
    treeBalance = treeBalance.add(commissionFeeMin);
  }
  const leavesWithCommission = leaves.concat(commissionLeaf);

  const allNodes = leavesWithCommission.map((leaf) => {
    const hash = getLeafHash(leaf, hashTypes, hashKeys);

    return {
      index: numNodes++,
      data: leaf,
      hash: hash,
      parentIndex: -1,
      leftChildIndex: -1,
      rightChildIndex: -1,
    };
  });

  let currentRow = allNodes.map((x) => x.index);
  let parentRow = [];
  const allRows = [];
  while (currentRow.length > 1) {
    // console.log({ currentRow });
    // handle case where the tree is not binary by adding zero hash
    if (currentRow.length % 2) {
      allNodes.push({
        index: numNodes++,
        hash: zeroHash,
        data: null,
        parentIndex: -1,
        leftChildIndex: -1,
        rightChildIndex: -1,
      });
      currentRow.push(numNodes - 1);
    }
    // loop over current row
    for (let i = 0; i < currentRow.length; i += 2) {
      const [pHash, flip] = parentHash(
        allNodes[currentRow[i]].hash,
        allNodes[currentRow[i + 1]].hash
      );
      const newNode = {
        index: numNodes++,
        hash: pHash,
        data: null,
        parentIndex: -1,
        leftChildIndex: flip
          ? allNodes[currentRow[i + 1]].index
          : allNodes[currentRow[i]].index,
        rightChildIndex: flip
          ? allNodes[currentRow[i]].index
          : allNodes[currentRow[i + 1]].index,
      };
      allNodes[currentRow[i]].parentIndex = numNodes - 1;
      allNodes[currentRow[i + 1]].parentIndex = numNodes - 1;
      allNodes.push(newNode);
      parentRow.push(newNode.index);
    }
    // keep track of rows
    allRows.push(currentRow);
    // move pointer to next row
    currentRow = parentRow;
    // zero out parent
    parentRow = [];
  }
  allRows.push(currentRow);
  return {
    rows: allRows,
    nodes: allNodes,
    root: allNodes[currentRow],
    treeBalance: treeBalance.toString(),
  };
}

function generateRandomData(numLeaves) {
  const addressLeaves = [
    { address: "0x90F8bf6A479f320ead074411a4B0e7944Ea8c9C1" },
  ];
  const metadataLeaves = [{ uri: "blahblahblah", tokenId: "1" }];

  for (let i = 0; i < numLeaves; i++) {
    if (i % 10000 == 0) {
      console.log({ i });
    }
    const address = { address: `0x${genRanHex(40)}` };
    const metadata = {
      uri: "ipfs://" + `0x${genRanHex(40)}`,
      tokenId: `0x${genRanHex(40)}`,
    };
    addressLeaves.push(address);
    metadataLeaves.push(metadata);
  }

  return { addressLeaves, metadataLeaves };
}

const contracts = {
  MerkleResistor: {
    type: "resistor",
    hashTypes: ["address", "uint", "uint"],
    hashKeys: ["destination", "minTotalPayments", "maxTotalPayments"],
  },
  MerkleDropFactory: {
    type: "airdrop",
    hashTypes: ["address", "uint"],
    hashKeys: ["destination", "value"],
  },
  MerkleVesting: {
    type: "vesting",
    hashTypes: ["address", "uint", "uint", "uint", "uint"],
    hashKeys: [
      "destination",
      "totalCoins",
      "startTime",
      "endTime",
      "lockPeriodEndTime",
    ],
  },
};

const parseCSVDataToJSON = (
  leaves,
  formType,
  friendlyValues = false,
  decimalPlaces = 18
) => {
  const lines = leaves.split(/\r\n|\n/);
  const preparedData = [];
  const headersIncluded = lines[0].includes("destination");
  const start = headersIncluded ? 1 : 0;
  for (let i = start; i < lines.length; i++) {
    if (formType === "resistor") {
      const [destination, minTotalPayments, maxTotalPayments] = lines[i]
        .replaceAll(" ", "")
        .split(",");
      if (!destination || !minTotalPayments || !maxTotalPayments) continue;

      const [_minTotalPayments, _maxTotalPayments] = parseFieldsPerContract(
        [minTotalPayments, maxTotalPayments],
        friendlyValues,
        decimalPlaces
      );

      preparedData.push({
        destination,
        minTotalPayments: _minTotalPayments,
        maxTotalPayments: _maxTotalPayments,
      });
    } else if (formType === "airdrop") {
      const [destination, value] = lines[i].replaceAll(" ", "").split(",");
      if (!destination || !value) continue;

      const [_value] = parseFieldsPerContract(
        [value],
        friendlyValues,
        decimalPlaces
      );

      preparedData.push({
        destination,
        value: _value,
      });
    } else {
      const [destination, totalCoins, startTime, endTime, lockPeriodEndTime] =
        lines[i].replaceAll(" ", "").split(",");

      const [_totalCoins] = parseFieldsPerContract(
        [totalCoins],
        friendlyValues,
        decimalPlaces
      );

      preparedData.push({
        destination,
        totalCoins: _totalCoins,
        startTime,
        endTime,
        lockPeriodEndTime,
      });
    }
  }
  return preparedData;
};

const parseLeavesValuesFromFile = async (
  file,
  type,
  friendlyValues = false,
  decimalPlaces = 18
) => {
  try {
    const leaves = await fs.promises.readFile(file.path, "utf-8");
    if (leaves) {
      let parsedLeaves;
      if (file.mimetype.includes("csv")) {
        parsedLeaves = parseCSVDataToJSON(
          leaves,
          type,
          friendlyValues,
          decimalPlaces
        );
      } else {
        //JSON CASE
        const data = JSON.parse(leaves);
        parsedLeaves =
          friendlyValues || decimalPlaces !== 18
            ? parseLeavesValues(data, type, friendlyValues, decimalPlaces)
            : data;
      }

      return parsedLeaves;
    }
  } catch (error) {
    console.log(error);
  }
};
async function createMerkleProofForAddress(target, targetKey, hash) {
  // TODO: this is linear, probably should make it a hash map to make it constant
  const regExp = new RegExp(target, "i");

  const query = targetKey.length
    ? { [`data.${targetKey}`]: regExp }
    : { hash: target };
  const [leaf] = await mongoose.connection
    .collection(hash)
    .find(query)
    .toArray();
  let x = { ...leaf };
  const proof = [];
  while (x.parentIndex > -1) {
    const parent = (
      await mongoose.connection
        .collection(hash)
        .find({ index: x.parentIndex })
        .toArray()
    )[0];
    const flip = x.index === parent.leftChildIndex;
    if (flip) {
      proof.push(
        (
          await mongoose.connection
            .collection(hash)
            .find({ index: parent.rightChildIndex })
            .toArray()
        )[0].hash
      );
    } else {
      proof.push(
        (
          await mongoose.connection
            .collection(hash)
            .find({ index: parent.leftChildIndex })
            .toArray()
        )[0].hash
      );
    }
    x = parent;
  }
  return proof;
}

module.exports = {
  createMerkleProof,
  checkMerkleProof,
  createMerkleTree,
  generateRandomData,
  parseLeavesValuesFromFile,
  createMerkleProofForAddress,
  contracts,
  parseLeavesValues,
  isValidLeaves,
};
