const redis = require("redis");

const redisClient = redis.createClient({
  socket: { host: "cache", port: 6379 },
});

redisClient.on("error", (err) => console.log("Redis Client Error", err));

redisClient.on("connect", function () {
  console.log("Redis client connected!");
});

redisClient.connect();

module.exports = {
  redisClient,
};
