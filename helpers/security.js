const { verifyMessage } = require('../helpers/signature');
const { Contract, providers } = require('ethers');
const networks = require('../helpers/networks.json');
const { MERKLE_CONTRACTS } = require('./utils');
const { redisClient } = require('./redis');
const RedisStore = require('rate-limit-redis');
const rateLimit = require('express-rate-limit');

const envTimeout = parseInt(process.env.RATE_LIMITER_TIMEOUT) || 1000 * 60 * 15; // default 15 minutes

const allowList = ['::ffff:172.22.0.1']; // allow localhost requests

const rateConfig = {
  windowMs: envTimeout, // Time frame limit
  max: 300, // Limit each IP to 100 requests per time frame
  standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
  legacyHeaders: false, // Disable the `X-RateLimit-*` headers
  store: new RedisStore({
    sendCommand: (...args) => redisClient.sendCommand(args)
  }),
  skip: (req, res) => allowList.includes(req.ip) // whitelist
};

const rateLimiter = rateLimit(rateConfig);

const treeLimiter = rateLimit({
  ...rateConfig,
  windowMs: 1000 * 60 * 5,
  max: 50
});

const hasAccessMiddleware = async (req, res, next) => {
  try {
    // const network = process.env.DEFAULT_NETWORK || 3;
    const { sig, msg } = req.body;
    if (msg.additionalData.showcase) {
      return next();
    }
    const CHAIN_ID = msg.additionalData.chainId;
    const provider = new providers.JsonRpcProvider(networks[CHAIN_ID].rpc[0]);
    const ContractAbi = MERKLE_CONTRACTS[msg?.additionalData?.contract];
    const contractAddress = ContractAbi.networks[CHAIN_ID].address;
    const contract = new Contract(contractAddress, ContractAbi.abi, provider);
    const managementAddress = await contract.management();
    if (!verifyMessage(managementAddress, JSON.stringify(msg), sig)) {
      return res.status(403).json({
        success: false,
        message: "You don't have access to that resource"
      });
    }
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  hasAccessMiddleware,
  rateLimiter,
  treeLimiter
};
