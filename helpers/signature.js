const { utils } = require("ethers");

module.exports.verifyMessage = function (address, msg, sig) {
  const recovered = utils.verifyMessage(msg, sig);
  console.log("recovered", recovered);
  console.log("address", address);
  return recovered === utils.getAddress(address);
};
