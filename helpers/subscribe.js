const MerkleTreeModel = require("../models/MerkleTree");
const MerkleResistor = require("../contracts/MerkleResistor");
const MerkleDropFactory = require("../contracts/MerkleDropFactory");
const MerkleVesting = require("../contracts/MerkleVesting");
const MerkleResistorV2 = require("../contracts/MerkleResistorV2");
const MerkleDropFactoryV2 = require("../contracts/MerkleDropFactoryV2");
const MerkleVestingV2 = require("../contracts/MerkleVestingV2");
const { fromWei, toWei, getProvider } = require("./utils");
const MerkleEvent = require("../models/MerkleEvent");
const { RedisEvents } = require("servment");

let eventsHelper;
function getDefaultSub() {
  if (!eventsHelper)
    eventsHelper = new RedisEvents(
      process.env.REDIS_SERVICE_URL || 'connector:6380'
    );
  return eventsHelper;
}

const CONTRACT_TYPES = {
  MerkleResistor: {
    name: "MerkleResistor",
    json: MerkleResistor,
    TokensDeposited: ["merkleIndex", "tokenAddress", "amountDeposited"],
    MerkleTreeAdded: ["merkleIndex", "tokenAddress", "newRoot", "ipfsHash"],
    WithdrawalOccurred: [
      "destination",
      "numTokens",
      "tokensLeft",
      "merkleIndex",
    ],
  },
  MerkleDropFactory: {
    name: "MerkleDropFactory",
    json: MerkleDropFactory,
    MerkleTreeAdded: ["merkleIndex", "tokenAddress", "newRoot", "ipfsHash"],
    Withdraw: ["merkleIndex", "recipent", "recipient"],
    TokensDeposited: ["merkleIndex", "tokenAddress", "amountDeposited"],
  },
  MerkleVesting: {
    name: "MerkleVesting",
    json: MerkleVesting,
    MerkleRootAdded: ["merkleIndex", "tokenAddress", "newRoot"],
    WithdrawalOccurred: [
      "destination",
      "numTokens",
      "tokensLeft",
      "merkleIndex",
    ],
    TokensDeposited: ["merkleIndex", "tokenAddress", "amountDeposited"],
  },
  MerkleResistorV2: {
    name: "MerkleResistor",
    json: MerkleResistorV2,
    TokensDeposited: ["merkleIndex", "tokenAddress", "amountDeposited"],
    MerkleTreeAdded: ["merkleIndex", "tokenAddress", "newRoot", "ipfsHash"],
    WithdrawalOccurred: [
      "merkleIndex",
      "destination",
      "numTokens",
      "tokensLeft",
    ],
  },
  MerkleDropFactoryV2: {
    name: "MerkleDropFactory",
    json: MerkleDropFactoryV2,
    MerkleTreeAdded: ["merkleIndex", "tokenAddress", "newRoot", "ipfsHash"],
    WithdrawalOccurred: ["merkleIndex", "recipent", "value"],
    TokensDeposited: ["merkleIndex", "tokenAddress", "amountDeposited"],
  },
  MerkleVestingV2: {
    name: "MerkleVesting",
    json: MerkleVestingV2,
    MerkleRootAdded: ["merkleIndex", "tokenAddress", "newRoot"],
    WithdrawalOccurred: [
      "merkleIndex",
      "destination",
      "numTokens",
      "tokensLeft",
    ],
    TokensDeposited: ["merkleIndex", "tokenAddress", "amountDeposited"],
  },
};

const CONTRACT_VERSIONS = {
  MerkleResistor: {
    1: { abi: MerkleResistor, name: "MerkleResistor" },
    2: { abi: MerkleResistorV2, name: "MerkleResistorV2" },
  },
  MerkleDropFactory: {
    1: { abi: MerkleDropFactory, name: "MerkleDropFactory" },
    2: { abi: MerkleDropFactoryV2, name: "MerkleDropFactoryV2" },
  },
  MerkleVesting: {
    1: { abi: MerkleVesting, name: "MerkleVesting" },
    2: { abi: MerkleVestingV2, name: "MerkleVestingV2" },
  },
};

async function initSubscriber(
  chainId,
  abi,
  abiName,
  eventName,
  setupOnly,
  query = {},
  callback = (events) => {}
) {
  if (!chainId || !abi || !abiName || !eventName) {
    console.log("Missing params in initSubscriber");
    return;
  }

  const abiNetwork = abi.networks[chainId];
  const contractAddress = abiNetwork.address;
  const fromBlock = abiNetwork.fromBlock;
  const events = getDefaultSub();
  const abiEvents = events.getEventsFromAbi(abi);
  await events.getAllEvents({
    project: abiName,
    chainId,
    eventName,
    requestBody: {
      abi: abiEvents,
      params: {
        contractAddress,
        fromBlock,
        query: {
          fromBlock,
          args: query,
        },
      },
      setupOnly,
    },
    callback,
  });
}

async function subscribeToTokensDeposited(network, contractType, version = 1) {
  try {
    const eventName = "TokensDeposited";
    console.log(`Getting ${eventName} events from:
      contract: ${contractType}
      version: ${version}
      network: ${network}
    `);
    const contract = CONTRACT_TYPES[contractType].json;
    const abiNetwork = contract.networks[network];
    if (!abiNetwork) {
      console.log(
        `Network:${network} not found for contract: ${contractType} version: ${version} event ${eventName}`
      );
      return;
    }
    const contractAddress = abiNetwork.address;
    console.log(`Subscribing to ${eventName} event on:
  contract: ${contractType}
  version: ${version}
  network: ${network}
  `);
    await initSubscriber(network, contract, contractType, eventName, true);

    const events = getDefaultSub();
    events.initializeEventListening(
      async (eventData) => {
        try {
          //TODO:Add handling for all events in contracts with further methods
          //- for now destructurizing all possible types of fields
          const { data } = eventData;
          let merkleIndex, amountDeposited;
          if (version === 1) {
            merkleIndex = data.index;
            amountDeposited = data.amount;
          } else {
            merkleIndex = data.treeIndex;
            amountDeposited = data.amount;
          }

          console.log(
            `${eventName} event captured for ${contractType} version: ${version} contract:
          tree index: ${merkleIndex.toString()}`
          );
          const tree = await MerkleTreeModel.findOne({
            treeIndex: parseInt(merkleIndex),
            "additionalData.contract": CONTRACT_TYPES[contractType].name,
            "additionalData.chainId": network,
            version,
          });
          const newBalance =
            Number(fromWei(tree.additionalData.tokenBalance)) +
            Number(fromWei(amountDeposited));
          const newAdditionalData = {
            ...tree.additionalData,
            tokenBalance: toWei(newBalance.toString()),
          };
          tree.additionalData = newAdditionalData;
          tree.markModified("additionalData.tokenBalance");
          await tree.save();
        } catch (error) {
          console.log(error);
        }
      },
      network,
      contractAddress,
      [eventName]
    );
  } catch (err) {
    console.error("Error in subscribeToTokensDeposited", err);
  }
}

async function subscribeToMerkleResistor(network, version = 1) {
  try {
    console.log(
      `Subscribing to MerkleResistor event: MerkleTreeAdded chain:${network} version:${version}`
    );
    const eventName = "MerkleTreeAdded";
    const contractVersion = CONTRACT_VERSIONS["MerkleResistor"][version];
    const resistorAbi = contractVersion.abi;
    const abiNetwork = resistorAbi.networks[network];
    if (!abiNetwork) {
      console.log(
        `Network:${network} not found for MerkleResistor event: MerkleTreeAdded version: ${version}`
      );
      return;
    }
    const merkleResistorAddress = abiNetwork.address;
    await initSubscriber(
      network,
      resistorAbi,
      contractVersion.name,
      eventName,
      true
    );

    const events = getDefaultSub();
    events.initializeEventListening(
      async (eventData) => {
        const { data } = eventData;
        let merkleIndex, address, newRoot;
        if (version === 1) {
          merkleIndex = data.index;
          address = data.tokenAddress;
          newRoot = data.newRoot;
        } else {
          merkleIndex = data.treeIndex;
          address = data.tokenAddress;
          newRoot = data.newRoot;
        }
        console.log(
          `MerkleTreeAdded event captured for MerkleResistor contract:
      tree index: ${merkleIndex.toString()}
      address: ${address}`
        );
        const localTree = await MerkleTreeModel.findOne({
          "root.hash": newRoot,
          "additionalData.contract": "MerkleResistor",
          treeIndex: { $exists: false },
        });
        if (localTree) {
          localTree.treeIndex = merkleIndex;
          await localTree.save();
        } else {
          console.log(
            `MerkleResistor tree with root hash ${newRoot} not found`
          );
        }
      },
      network,
      merkleResistorAddress,
      eventName
    );
  } catch (err) {
    console.error("Error in subscribeToMerkleResistor", err);
  }
}

async function addWithdrawEvent(
  owner,
  merkleIndex,
  amountWithdraw,
  transactionHash,
  blockNumber,
  provider,
  contractName,
  chainId,
  version
) {
  const [withdrawnBlock, transaction] = await Promise.all([
    provider.getBlock(blockNumber, chainId),
    MerkleEvent.findOne({
      transactionHash,
    }),
  ]);

  if (!transaction) {
    await MerkleEvent.create({
      treeIndex: parseInt(merkleIndex),
      contract: contractName,
      timeWithdrawn: withdrawnBlock.timestamp,
      amountWithdraw,
      owner,
      blockNumber,
      chainId,
      transactionHash,
      version,
    });
  }

  return !transaction;
}

async function getWithdrawEvents(
  chainId,
  provider,
  contractName,
  eventName,
  version
) {
  let trees = await MerkleTreeModel.find({
    treeIndex: { $exists: true },
    "additionalData.contract": contractName,
    "additionalData.chainId": chainId,
    version,
  });

  for (const tree of trees) {
    const lastWithdrawnBlock = tree.additionalData.lastWithdrawnBlock ?? 0;
    const query =
      version === 2
        ? { treeIndex: tree.treeIndex }
        : { merkleIndex: tree.treeIndex };
    const currentTreeBalance =
      Number(fromWei(tree.additionalData.treeBalance)) ?? 0;

    const contractVersion = CONTRACT_VERSIONS[contractName][version];
    await initSubscriber(
      chainId,
      contractVersion.abi,
      contractVersion.name,
      eventName,
      false,
      query,
      async (data) => {
        if (!data) return;
        const { success, allEvents: withdrawEvents } = data;
        console.log(
          `Sub initialize for chain:${chainId} name:${eventName} project:${
            contractVersion.name
          } contractAddress:${
            contractVersion.abi.networks?.[chainId]?.address
          } ${success ? "succeded" : "failed"} `
        );
        console.log(
          `Found ${withdrawEvents?.length} new events on ${contractName} version ${version} for treeIndex ${tree.treeIndex}`
        );
        if (!success || !withdrawEvents?.length) return;
        let sumNumTokens = 0;
        let newLastWithdrawnBlock = 0;
        
        const ownerField =
          contractName === "MerkleDropFactory" && version === 1
            ? "recipient"
            : "destination";
        const amountField =
          contractName === "MerkleDropFactory" ? "value" : "numTokens";
        const treeIndex = version === 2 ? "treeIndex" : "merkleIndex";
        
        for (const event of withdrawEvents) {

          const {
            [ownerField]: owner,
            [treeIndex]: merkleIndex,
            [amountField]: amount,
          } = event.data;

          const { transactionHash, blockNumber } = event;
          const amountWithdraw = Number(fromWei(amount));

          const isNewEvent = await addWithdrawEvent(
            owner,
            merkleIndex,
            amount.toString(),
            transactionHash,
            blockNumber,
            provider,
            contractName,
            chainId,
            version
          );

          sumNumTokens += isNewEvent ? amountWithdraw : 0;
          newLastWithdrawnBlock = isNewEvent ? blockNumber : 0;
        }

        if (
          sumNumTokens > 0 &&
          newLastWithdrawnBlock > 0 &&
          newLastWithdrawnBlock > lastWithdrawnBlock
        ) {
          const newTreeBalance = currentTreeBalance - sumNumTokens;
          const newAdditionalData = {
            ...tree.additionalData,
            treeBalance: toWei(newTreeBalance.toString()),
            lastWithdrawnBlock: newLastWithdrawnBlock,
          };

          tree.additionalData = newAdditionalData;
          tree.markModified("additionalData.treeBalance");
          tree.markModified("additionalData.lastWithdrawnBlock");
          await tree.save();
        }
      }
    );
  }
}

function calcNewTreeBalance(neededTreeBalance, withdrawValue) {
  const _neededTreeBalance = Number(fromWei(neededTreeBalance));
  const _withdrawValue = Number(fromWei(withdrawValue));
  const balanceDiff = _neededTreeBalance - _withdrawValue;

  return toWei(balanceDiff.toString());
}

async function subscribeToMerkleVesting(network, version = 1) {
  try {
    console.log(
      `Subscribing to MerkleVesting event: MerkleRootAdded chain:${network} version:${version}`
    );
    const contractVersion = CONTRACT_VERSIONS["MerkleVesting"][version];
    const vestingAbi = contractVersion.abi;
    const abiNetwork = vestingAbi.networks[network];
    if (!abiNetwork) {
      console.log(
        `Network:${network} not found for MerkleVesting event: MerkleTreeAdded version: ${version}`
      );
      return;
    }
    const merkleVestingAddress = abiNetwork.address;
    await initSubscriber(
      network,
      vestingAbi,
      contractVersion.name,
      "MerkleRootAdded",
      true
    );

    const events = getDefaultSub();
    events.initializeEventListening(
      async (eventData) => {
        const { data } = eventData;
        let merkleIndex, address, newRoot;
        if (version === 1) {
          merkleIndex = data.index;
          address = data.tokenAddress;
          newRoot = data.newRoot;
        } else {
          merkleIndex = data.treeIndex;
          address = data.tokenAddress;
          newRoot = data.newRoot;
        }
        console.log(
          `MerkleRootAdded event captured for MerkleVesting contract:
      tree index: ${merkleIndex.toString()}
      address: ${address}`
        );

        const localTree = await MerkleTreeModel.findOne({
          "root.hash": newRoot,
          "additionalData.contract": "MerkleVesting",
          treeIndex: { $exists: false },
        });
        if (localTree) {
          localTree.treeIndex = merkleIndex;
          await localTree.save();
        } else {
          console.log(`MerkleVesting tree with root hash ${newRoot} not found`);
        }
      },
      network,
      merkleVestingAddress,
      ["MerkleRootAdded"]
    );
  } catch (err) {
    console.error("Error in subscribeToMerkleVesting", err);
  }
}

async function subscribeToMerkleDropFactory(network, version = 1) {
  try {
    console.log(
      `Subscribing to MerkleDropFactory event: MerkleTreeAdded chain:${network} version:${version}`
    );
    const contractVersion = CONTRACT_VERSIONS["MerkleDropFactory"][version];
    const dropAbi = contractVersion.abi;
    const abiNetwork = dropAbi.networks[network];
    if (!abiNetwork) {
      console.log(
        `Network:${network} not found for MerkleDropFactory event: MerkleTreeAdded version: ${version}`
      );
      return;
    }
    const merkleDropFactoryAddress = abiNetwork.address;
    await initSubscriber(
      network,
      dropAbi,
      contractVersion.name,
      "MerkleTreeAdded",
      true
    );

    const events = getDefaultSub();
    events.initializeEventListening(
      async (eventData) => {
        const { data } = eventData;
        const address = data.tokenAddress;
        const newRoot = data.newRoot;
        let merkleIndex;
        if (version === 1) {
          merkleIndex = data.index;
        } else {
          merkleIndex = data.treeIndex;
        }
        console.log(
          `MerkleTreeAdded event captured for MerkleDropFactory contract:
      tree index: ${merkleIndex.toString()}
      address: ${address}`
        );

        const localTree = await MerkleTreeModel.findOne({
          "root.hash": newRoot,
          "additionalData.contract": "MerkleDropFactory",
          treeIndex: { $exists: false },
        });
        if (localTree) {
          localTree.treeIndex = merkleIndex;
          await localTree.save();
        } else {
          console.log(
            `MerkleDropFactory tree with root hash ${newRoot} not found`
          );
        }
      },
      network,
      merkleDropFactoryAddress,
      ["MerkleTreeAdded"]
    );
  } catch (err) {
    console.error("Error in subscribeToMerkleDropFactory", err);
  }
}

async function subscribeMerkleWithdraw(
  chainId,
  merkle,
  contractName,
  version = 1,
  eventName = "WithdrawalOccurred"
) {
  try {
    console.log(`Getting ${eventName} events from:
  contract: ${contractName}
  version: ${version}
  network: ${chainId}
  `);

    const abiNetwork = merkle.networks[chainId];
    if (!abiNetwork) {
      console.log(
        `Network:${chainId} not found for contract: ${contractName} version: ${version} event Withdraw`
      );
      return;
    }
    const contractAddress = abiNetwork.address;
    const provider = getProvider(chainId);
    await getWithdrawEvents(
      chainId,
      provider,
      contractName,
      eventName,
      version
    );

    console.log(`Subscribing to ${eventName} event on:
  contract: ${contractName}
  version: ${version}
  network: ${chainId}
  `);
    const events = getDefaultSub();
    events.initializeEventListening(
      async (eventData) => {
        try {
          const { data, blockNumber, transactionHash } = eventData;
          const ownerField =
            contractName === "MerkleDropFactory" && version === 1
              ? "recipient"
              : "destination";
          const amountField =
            contractName === "MerkleDropFactory" ? "value" : "numTokens";
          const treeIndex = version === 2 ? "treeIndex" : "merkleIndex";

          const {
            [ownerField]: owner,
            [treeIndex]: merkleIndex,
            [amountField]: amountWithdraw,
          } = data;

          console.log(
            `${eventName} event captured for ${contractName} contract:
        tree index: ${merkleIndex.toString()} 
        version: ${version}
        address: ${owner}`
          );

          const tree = await MerkleTreeModel.findOne({
            treeIndex: parseInt(merkleIndex),
            "additionalData.contract": contractName,
            "additionalData.chainId": chainId,
            version,
          });
          const provider = getProvider(chainId);
          const isNewEvent = await addWithdrawEvent(
            owner,
            merkleIndex,
            amountWithdraw.toString(),
            transactionHash,
            blockNumber,
            provider,
            contractName,
            chainId,
            version
          );

          if (isNewEvent) {
            const newTreeBalance = calcNewTreeBalance(
              tree.additionalData.treeBalance,
              amountWithdraw
            );

            const newAdditionalData = {
              ...tree.additionalData,
              treeBalance: newTreeBalance,
              lastWithdrawnBlock: blockNumber,
            };

            tree.additionalData = newAdditionalData;
            tree.markModified("additionalData.treeBalance");
            tree.markModified("additionalData.lastWithdrawnBlock");
            await tree.save();
          }
        } catch (error) {
          console.log(error);
        }
      },
      chainId,
      contractAddress,
      [eventName]
    );
  } catch (err) {
    console.error("Error in subscribeMerkleWithdraw", err);
  }
}

async function subscribeToV1Events(chainId) {
  subscribeToMerkleResistor(chainId, 1);
  subscribeToMerkleDropFactory(chainId, 1);
  subscribeToMerkleVesting(chainId, 1);

  subscribeMerkleWithdraw(chainId, MerkleVesting, "MerkleVesting", 1);
  subscribeMerkleWithdraw(chainId, MerkleResistor, "MerkleResistor", 1);
  subscribeMerkleWithdraw(
    chainId,
    MerkleDropFactory,
    "MerkleDropFactory",
    1,
    "Withdraw"
  );

  subscribeToTokensDeposited(chainId, "MerkleResistor", 1);
}

async function subscribeToV2Events(chainId) {
  subscribeToMerkleResistor(chainId, 2);
  subscribeToMerkleDropFactory(chainId, 2);
  subscribeToMerkleVesting(chainId, 2);

  subscribeMerkleWithdraw(chainId, MerkleVestingV2, "MerkleVesting", 2);
  subscribeMerkleWithdraw(chainId, MerkleResistorV2, "MerkleResistor", 2);
  subscribeMerkleWithdraw(chainId, MerkleDropFactoryV2, "MerkleDropFactory", 2);
  subscribeToTokensDeposited(chainId, "MerkleResistorV2", 2);
}

// let allowResubscribe = false;

// function initResubscribeOnDisconnect(){
//   const events = getDefaultSub();
//   events.socket.on("connect", () => {
//     if(allowResubscribe){
//       console.log('Resubscribing -----------------------------');
//       activeSubscribers = {};
//       subscribeToRootCreation();
//       allowResubscribe = false;
//     }
//   });
//   events.socket.on("disconnect", () => {
//     allowResubscribe = true;
//   });
// }

const activeSubscribers = {};

async function subscribeToSingleRootCreation(chainId) {
  if (activeSubscribers[chainId]) {
    console.log(`Subscriber already active for chain:${chainId}`);
    return;
  }
  await Promise.all([
    subscribeToV1Events(chainId),
    subscribeToV2Events(chainId),
  ]);
  activeSubscribers[chainId] = true;
}

async function subscribeToRootCreation() {
  const currentChains = await MerkleTreeModel.distinct(
    "additionalData.chainId",
    {
      treeIndex: { $exists: true },
    }
  );
  if (!currentChains.length) return;
  //initResubscribeOnDisconnect();
  for (let chainId of currentChains) {
    subscribeToSingleRootCreation(chainId);
  }
}

module.exports = {
  subscribeToRootCreation,
  subscribeToSingleRootCreation,
};
