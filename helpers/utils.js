const { providers, Contract } = require("ethers");
const MerkleDropFactory = require("../contracts/MerkleDropFactory");
const MerkleResistor = require("../contracts/MerkleResistor");
const MerkleVesting = require("../contracts/MerkleVesting");
const networks = require("./networks.json");
const { utils } = require("ethers");
const Token = require("../contracts/Token");

const MERKLE_CONTRACTS = {
  MerkleDropFactory,
  MerkleResistor,
  MerkleVesting,
};

const getContractTreeData = async (
  treeIndex,
  { contract, treeBalance, chainId, project, depositToken }
) => {
  try {
    const { networks, abi } = MERKLE_CONTRACTS[contract];
    const provider = getProvider(chainId);
    const contractInstance = new Contract(
      networks[chainId].address,
      abi,
      provider
    );
    const { tokenBalance } = await contractInstance.merkleTrees(treeIndex);
    return {
      tokenBalance: tokenBalance.toString(),
      treeBalance,
      chainId,
      treeIndex,
      contract,
      project,
      depositToken,
    };
  } catch (e) {
    return Promise.reject(e);
  }
};

const fromWei = (num) => {
  return utils.formatEther(num);
};

const toWei = (num) => {
  return utils.parseEther(num).toString();
};

const parseUnit = (value, unit) => {
  return utils.parseUnits(value, unit);
};

const REQUIRE_PERCENTAGE = process.env.DEFAULT_PERCENTAGE || 60;

const currentProviders = {};
function getProvider(network) {
  const url = networks[network].rpc[0];
  if (!currentProviders[network])
    currentProviders[network] = new providers.JsonRpcProvider(url);
  return currentProviders[network];
}

async function getDecimalPlaces(tokenAddress, chainId) {
  if (!tokenAddress) return;

  const provider = getProvider(chainId);
  const tokenContract = new Contract(tokenAddress, Token.abi, provider);
  return await tokenContract.decimals();
}

module.exports = {
  MERKLE_CONTRACTS,
  REQUIRE_PERCENTAGE,
  fromWei,
  toWei,
  getContractTreeData,
  getProvider,
  parseUnit,
  getDecimalPlaces,
};
