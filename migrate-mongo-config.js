const dotenv = require("dotenv");
const mongoose = require("mongoose");
const { database } = require("./config");
dotenv.config();

// In this file you can configure migrate-mongo
const connect = function () {
  mongoose
    .connect(`mongodb://${database.host}:${database.port}/${database.name}`, {})
    .catch((e) => {
      console.error("Connection error", e.message);
    });
};
connect();
module.exports = {
  mongodb: {
    url: `mongodb://${database.host}:${database.port}/`,
    databaseName: database.name,

    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },

  migrationsDir: "migrations",

  changelogCollectionName: "changelog",
  migrationFileExtension: ".js",
  useFileHash: false,
  moduleSystem: "commonjs",
};
