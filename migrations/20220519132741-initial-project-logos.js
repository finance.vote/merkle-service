const ProjectModel = require("../models/Project");
const projects = [
  ["fvt", "finance.svg"],
  ["cudos", "DL_airdrop.png"],
  ["totm", "DL_airdrop.png"],
  ["bump", "DL_airdrop.png"],
  ["hid", "DL_airdrop.png"],
  ["l3p", "DL_airdrop.png"],
  ["metis", "DL_airdrop.png"],
  ["oly", "oly-logo.svg"],
];
module.exports = {
  async up(db, client) {
    for await (const [projectKey, filename] of projects) {
      const key = new RegExp(projectKey, "i");
      const imagePath = "/home/node/app/images/" + filename;
      await ProjectModel.updateOne({ key }, { logo: imagePath });
    }

    // TODO write your migration here.
    // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: true}});
  },

  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  },
};
