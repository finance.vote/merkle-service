const { Schema, model } = require("mongoose");

const MerkleEvent = new Schema(
  {
    transactionHash: { type: String },
    treeIndex: { type: Number },
    contract: { type: String },
    chainId: { type: Number },
    timeWithdrawn: { type: Number },
    blockWithdrawn: { type: Number },
    owner: { type: String },
    amountWithdraw: { type: String },
    version: { type: Number },
  },
  {
    timestamps: true,
  }
);

module.exports = model("MerkleEvent", MerkleEvent);
