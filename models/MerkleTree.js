const { Schema, model } = require("mongoose");

const MerkleTreeSchema = new Schema(
  {
    treeIndex: { type: Number, index: true },
    version: { type: Number, required: true, default: 2 },
    project: { type: String, required: true },
    additionalData: { type: Schema.Types.Mixed },
    inputData: [{ type: Schema.Types.Mixed }],
    tree: Schema.Types.Mixed,
    root: {
      index: Number,
      hash: String,
      data: Schema.Types.Mixed,
      parentIndex: Number,
      leftChildIndex: Number,
      rightChildIndex: Number,
    },
  },
  {
    timestamps: true,
  }
);

MerkleTreeSchema.index({ treeIndex: 1, project: 1, queryData: 1 });

module.exports = model("MerkleTree", MerkleTreeSchema);
