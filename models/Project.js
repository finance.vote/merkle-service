const { Schema, model } = require("mongoose");

const Project = new Schema(
  {
    key: { type: String, unique: true },
    label: { type: String },
    logo: { type: String },
    symbol: { type: String },
    address: { type: Schema.Types.Mixed },
  },
  {
    timestamps: true,
  }
);

module.exports = model("Project", Project);
